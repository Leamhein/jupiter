import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{ path: '', pathMatch: 'full', redirectTo: 'map' },
	{
		path: 'map',
		loadChildren: () =>
			import('./containers/map/map.module').then(m => m.MapModule),
	},
	{
		path: '**',
		redirectTo: 'map',
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
