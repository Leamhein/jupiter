import { NgModule } from '@angular/core';

import { MapRoutingModule } from './map-routing.module';
import { MapComponent } from './map.component';

import { SharedModule } from '@shared/shared.module';
import { INIT_COORDS } from '@tokens';

@NgModule({
	declarations: [MapComponent],
	imports: [SharedModule, MapRoutingModule],
	providers: [
		{ provide: INIT_COORDS, useValue: { lat: 32.9756, long: -96.89 } },
	],
})
export class MapModule {}
