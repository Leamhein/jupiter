import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { SharedModule } from '@shared/shared.module';

const COMPONENTS = [];

@NgModule({
	imports: [BrowserModule, HttpClientModule, SharedModule],
	declarations: [...COMPONENTS],
	exports: [...COMPONENTS],
})
export class ContainersModule {}
